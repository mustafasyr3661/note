import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buildTextForm({
  @required String? hintText,
  @required IconData? prefixIcon,
  @required TextEditingController? controller,
  String? Function(String? val)? onSaved,
  bool isShow = false,
  String? Function(String? val)? validate,
  void Function()? onPressed,
  TextInputType? textInputType,
}) {
  return TextFormField(
      obscureText: isShow,
      validator: validate,
      onSaved: onSaved,
      controller: controller,
      keyboardType: textInputType,
      decoration: InputDecoration(
        hintText: hintText,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 2),
            borderRadius: BorderRadius.circular(20)),
        prefixIcon: IconButton(
          onPressed: onPressed,
          icon: Icon(prefixIcon),
        ),
        border: OutlineInputBorder(
            borderSide: BorderSide(width: 2),
            borderRadius: BorderRadius.circular(20)),
      ));
}

Widget buildButton(
    {@required String? text,
    @required Color? buttonColor,
    Color sideColor = Colors.black,
    Color textColor = Colors.white,
    Color shadowColor = Colors.black,
    @required var onPressed}) {
  return SizedBox(
    height: 50,
    width: double.infinity,
    child: ElevatedButton(
      onPressed: onPressed,
      child: Text(
        text!,
        style: TextStyle(fontSize: 20, color: textColor),
      ),
      style: ElevatedButton.styleFrom(
          shadowColor: shadowColor,
          primary: buttonColor,
          side: BorderSide(color: sideColor, width: 2),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
    ),
  );
}

buildLoadingItem(context) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Center(child: Text("Please wait")),
          content: SizedBox(
            height: 50,
            child: Center(
              child: CircularProgressIndicator(color: Colors.black87,),
            ),
          ),

        );
      });
}
