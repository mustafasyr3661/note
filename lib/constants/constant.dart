import 'package:flutter/cupertino.dart';
import 'package:note_app/presentation/authentication/login.dart';
import 'package:note_app/presentation/authentication/sign_up.dart';
import 'package:note_app/presentation/screens/add_a_note.dart';
import 'package:note_app/presentation/screens/home_page.dart';

const String loginRoute = "login";
const String signUpRoute = "signup";
const String homeRoute = "homePage";
const String addANoteRoute = "addANote";
const String userCollection ='users';
bool isLogin = false;

Map <String,Widget Function(BuildContext)>routesMap = {
  loginRoute: (context) => Login(),
  signUpRoute: (context) => SignUpScreen(),
  homeRoute: (context) => HomePage(),
  addANoteRoute: (context) => AddANote(),
};
