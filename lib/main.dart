import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:note_app/presentation/authentication/login.dart';
import 'package:note_app/presentation/authentication/sign_up.dart';
import 'package:note_app/presentation/screens/home_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'constants/constant.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  var userInfo = FirebaseAuth.instance.currentUser;
  if (userInfo == null) {
    isLogin = false;
  } else {
    isLogin = true;
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Colors.black,
          scaffoldBackgroundColor: Colors.white,
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
          ),
          appBarTheme: AppBarTheme(
              backgroundColor: Colors.black,
              systemOverlayStyle: SystemUiOverlayStyle(
                  statusBarColor: Colors.white,
                  statusBarIconBrightness: Brightness.dark))),
      home: isLogin ? HomePage() : Login(),
      routes: routesMap,
    );
  }
}
