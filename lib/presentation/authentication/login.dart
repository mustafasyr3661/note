import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note_app/constants/components.dart';
import 'package:note_app/constants/constant.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var userNameController = TextEditingController();
  var passwordController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  bool isPasswordShow = true;
  IconData passwordIcon = Icons.remove_red_eye_sharp;
  FirebaseAuth auth = FirebaseAuth.instance;
  UserCredential? userCredential;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "images/1.png",
                width: 200,
                height: 200,
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      buildTextForm(
                          hintText: "UserName Or Email",
                          prefixIcon: Icons.person,
                          controller: userNameController,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return "you have to enter your password";
                            }
                            return null;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      buildTextForm(
                          hintText: "password",
                          prefixIcon: passwordIcon,
                          isShow: isPasswordShow,
                          onPressed: () {
                            isPasswordShow = !isPasswordShow;
                            if (isPasswordShow) {
                              setState(() {
                                passwordIcon = Icons.remove_red_eye;
                              });
                            } else {
                              setState(() {
                                passwordIcon = Icons.remove_red_eye_outlined;
                              });
                            }
                          },
                          controller: passwordController,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return "you have to enter your password ";
                            }
                            return null;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        height: 50,
                        width: double.infinity,
                        child: buildButton(
                          text: "Long In",
                          buttonColor: Colors.black,
                          onPressed: () async {
                            UserCredential? user = await signInWithEmailAndPassword();
                            print("======");
                            if (user != null) {
                              Navigator.of(context).pushReplacementNamed(homeRoute);
                            } else {
                              print("error");
                            }
                            print("======");
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text("If you do nat have an account",
                              style: Theme.of(context).textTheme.headline1),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context)
                                  .pushReplacementNamed(signUpRoute);
                            },
                            child: Text("create one now",
                                style: Theme.of(context).textTheme.button),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  signInWithEmailAndPassword() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      try {
        buildLoadingItem(context);
        userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: userNameController.text, password: passwordController.text);
        return userCredential;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'user-not-found') {
          Navigator.of(context).pop();
          AwesomeDialog(
                  context: context,
                  title: "error",
                  body: Text("No user found for that email."),
                  padding: EdgeInsets.all(8))
              .show();
          print('No user found for that email.');
        } else if (e.code == 'wrong-password') {
          Navigator.of(context).pop();
          AwesomeDialog(
                  context: context,
                  title: "error",
                  body: Text("Wrong password provided for that user."),
                  padding: EdgeInsets.all(8))
              .show();
          print('Wrong password provided for that user.');
        }
      }
    }
  }
}
