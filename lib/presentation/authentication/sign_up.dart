
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note_app/constants/constant.dart';
import '../../constants/components.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  var firstNameController = TextEditingController();
  var passwordController = TextEditingController();
  var lastNameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var passwordConfirmController = TextEditingController();
  var usernameController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  bool isPasswordShow = true;
  bool isConfirmShow = true;
  IconData passwordIcon = Icons.remove_red_eye_sharp;
  UserCredential? userCredential;
  FirebaseFirestore fireStore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "images/1.png",
                width: 200,
                height: 200,
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: buildTextForm(
                                hintText: "FirstName",
                                prefixIcon: Icons.person,
                                controller: firstNameController,
                                validate: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Enter first name";
                                  }
                                  return null;
                                }),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: buildTextForm(
                                hintText: "LastName",
                                prefixIcon: Icons.person,
                                controller: lastNameController,
                                validate: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Enter last name";
                                  }
                                  return null;
                                }),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      buildTextForm(
                          hintText: "UserName",
                          prefixIcon: Icons.person,
                          controller: usernameController,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return "you have to enter your password";
                            }
                            if (value.length > 25) {
                              return "UserName can't be larger than 25 letters";
                            }
                            if (value.length < 10) {
                              return "UserName can't be less than 10 letters";
                            }
                            return null;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      buildTextForm(
                          hintText: "Email",
                          prefixIcon: Icons.email,
                          controller: emailController,
                          textInputType: TextInputType.emailAddress,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return "Choose an email address";
                            }
                            return null;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      buildTextForm(
                        hintText: "PhoneNumber",
                        prefixIcon: Icons.settings_phone_sharp,
                        controller: phoneController,
                        textInputType: TextInputType.phone,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      buildTextForm(
                          hintText: "password",
                          prefixIcon: passwordIcon,
                          textInputType: TextInputType.visiblePassword,
                          isShow: isPasswordShow,
                          onPressed: () {
                            isPasswordShow = !isPasswordShow;
                            if (isPasswordShow) {
                              setState(() {
                                passwordIcon = Icons.remove_red_eye;
                              });
                            } else {
                              setState(() {
                                passwordIcon = Icons.remove_red_eye_outlined;
                              });
                            }
                          },
                          controller: passwordController,
                          validate: (value) {
                            if (value == null || value.isEmpty) {
                              return "Enter a password";
                            }
                            if (value.length < 5) {
                              return "Password can't be less than 5 letters";
                            }
                            return null;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      buildTextForm(
                          hintText: "Confirm",
                          controller: passwordConfirmController,
                          textInputType: TextInputType.visiblePassword,
                          prefixIcon: passwordIcon,
                          isShow: isConfirmShow,
                          onPressed: () {
                            isConfirmShow = !isConfirmShow;
                            if (isConfirmShow) {
                              setState(() {
                                passwordIcon = Icons.remove_red_eye;
                              });
                            } else {
                              setState(() {
                                passwordIcon = Icons.remove_red_eye_outlined;
                              });
                            }
                          },
                          validate: (value) {
                            if (value != passwordController.text) {
                              return "Confirm your password ";
                            }
                            return null;
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      buildButton(
                        text: "Sing up",
                        buttonColor: Colors.black,
                        onPressed: () async {
                          UserCredential? user = await signUpWithFirebase();
                          print("======");
                          if (user != null) {
                            addUserInfoToFirebase();
                            Navigator.of(context)
                                .pushReplacementNamed(homeRoute);
                          } else {
                            print("error");
                          }
                          print("======");
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "- OR -",
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 20),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      buildButton(
                          text: "Google",
                          buttonColor: Colors.white,
                          onPressed: () {},
                          sideColor: Colors.red,
                          textColor: Colors.red,
                          shadowColor: Colors.red),
                      SizedBox(
                        height: 10,
                      ),
                      buildButton(
                          text: "Facebook",
                          buttonColor: Colors.white,
                          onPressed: () {},
                          sideColor: Colors.blue,
                          textColor: Colors.blue,
                          shadowColor: Colors.blue),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text("If you have an account",
                              style: Theme.of(context).textTheme.headline1),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacementNamed(loginRoute);
                              },
                              child: Text("login now",
                                  style: Theme.of(context).textTheme.button))
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<UserCredential?> signUpWithFirebase() async {
    var formData = formKey.currentState;
    if (formData!.validate()) {
      formData.save();
      try {
        buildLoadingItem(context);
        userCredential = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
                email: emailController.text, password: passwordController.text);
        return userCredential;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'weak-password') {
          Navigator.of(context).pop();
          AwesomeDialog(
                  context: context,
                  title: "error",
                  body: Text("The password provided is too weak."))
              .show();
          print('The password provided is too weak.');
        } else if (e.code == 'email-already-in-use') {
          Navigator.of(context).pop();
          AwesomeDialog(
                  context: context,
                  title: "error",
                  body: Text("The account already exists for that email."))
              .show();
          print('The account already exists for that email.');
        }
      } catch (e) {
        print(e);
      }
    }
  }

  addUserInfoToFirebase() async {
    CollectionReference userInfo = fireStore.collection(userCollection);
    userInfo.add({
      "email": emailController.text,
      "lastname":lastNameController.text,
      "name":firstNameController.text,
      "phone":phoneController.text,
      "username":usernameController.text

    });
  }
}
