import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:note_app/constants/components.dart';
import 'package:note_app/constants/constant.dart';

class AddANote extends StatelessWidget {
  AddANote({Key? key}) : super(key: key);
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  String? title;
  String? note;
  String? imageUrl;
  File? file;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add a new note"),
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
        child: Form(
          key: formState,
          child: Column(
            children: [
              TextFormField(
                onSaved: (value) {
                  title = value;
                },
                maxLength: 10,
                maxLines: 1,
                decoration: InputDecoration(
                    hintText: "Add note's title",
                    prefixIcon: Icon(Icons.sticky_note_2)),
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                onSaved: (value) {
                  note = value;
                },
                maxLength: 200,
                maxLines: 4,
                minLines: 1,
                decoration: InputDecoration(
                    hintText: "Type your note", prefixIcon: Icon(Icons.note)),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                      child: buildButton(
                          text: "Add image",
                          buttonColor: Colors.black,
                          onPressed: () {
                            showBottomSheet(context);
                          })),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: Container(
                    height: 100,
                    decoration: BoxDecoration(
                      border: Border.all(width: 2, color: Colors.black),
                      image: DecorationImage(
                          image: AssetImage("images/1.png"),
                          fit: BoxFit.contain),
                    ),
                  ))
                ],
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushReplacementNamed(homeRoute);
        },
        child: Icon(Icons.save),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
}

showBottomSheet(context) {
  return showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          height: 150,
          color: Color(0xFF737373),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(50),
                    topRight: Radius.circular(50))),
            child: Column(
              children: [
                Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(
                      "Please Choose an image",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    )),
                SizedBox(
                  height: 10,
                ),
                InkWell(
                  onTap: () async {
                    var picked = await ImagePicker()
                        .getImage(source: ImageSource.gallery);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.photo_outlined,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "From Gallery",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 20),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.camera_outlined,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "From camera",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 20),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      });
}
