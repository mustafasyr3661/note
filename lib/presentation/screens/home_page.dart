import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:note_app/constants/constant.dart';
import 'package:note_app/data/note.dart';
import 'package:note_app/presentation/widgets/note_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    getAllUserInfo();
    super.initState();
  }
  late User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: buildDrawerUi(),
      ),
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Colors.grey[900],
            statusBarIconBrightness: Brightness.light),
        title: Text("Notes"),
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: (){
              logoutBuilder();
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).pushNamed("addANote");
          },
          child: Icon(Icons.add),
          backgroundColor: Theme.of(context).primaryColor),
      body: Container(
        padding: EdgeInsets.all(8),
        child: ListView.builder(
          itemBuilder: (context, index) {
            return NoteItem(
              notes: notes[index],
            );
          },
          itemCount: notes.length,
        ),
      ),
    );
  }

  void getAllUserInfo() {
     user = FirebaseAuth.instance.currentUser as User;
  }

  Future<void> logoutBuilder() async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacementNamed(loginRoute);
  }

  Widget buildDrawerUi() {
    return Column(
      children:  [
        UserAccountsDrawerHeader(
          accountEmail: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "Email: ",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                TextSpan(
                  text: "${user.email}",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          accountName: Text("Name"),
        ),
        Text(''),
      ],
    );
  }
}
