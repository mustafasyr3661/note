import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NoteItem extends StatelessWidget {
  final Map notes;
  const NoteItem({Key? key,required this.notes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          Expanded(
              child: Image.asset(
                "images/1.png",
                width: 110,
                height: 110,
              )),
          Expanded(
            flex: 2,
            child: ListTile(
              title: Text("Title"),
              subtitle: Text(notes["note"]),
              trailing: Icon(Icons.edit),
            ),
          ),
        ],
      ),
    );
  }
}

